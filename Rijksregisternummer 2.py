import random
import datetime

# geboortedatum = Jaar - maand - dag = jj.mm.dd

start_date = datetime.date(1980, 1, 1)
end_date = datetime.date(2023, 1, 1)

time_between_dates = end_date - start_date
days_between_dates = time_between_dates.days
random_number_of_days = random.randrange(days_between_dates)
days = random_number_of_days
random_date = start_date + datetime.timedelta(days)

datum = str(random_date)
datum = datum.replace('-', '.')
datum = datum[2:]

# dagteller = xxx

teller = random.randrange(1, 998)

# controlegetal = cc

controle = datum + str(teller)
controle = controle.replace('.', '')
controle = int(controle)

c = 97 - (controle % 97)

# rijksregister = jj.mm.dd-xxx.cc

print(f'{datum}-{teller:03d}.{c:02d}')
