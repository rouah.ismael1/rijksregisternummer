import random
import time

# geboortedatum = Jaar - maand - dag = jj.mm.dd

def str_time_prop(start, end, time_format, prop):

    stime = time.mktime(time.strptime(start, time_format))
    etime = time.mktime(time.strptime(end, time_format))

    ptime = stime + prop * (etime - stime)

    return time.strftime(time_format, time.localtime(ptime))

def random_date(start, end, prop):
    return str_time_prop(start, end, '%Y.%m.%d', prop)

datum = str(random_date("1900.1.1", "2023.1.1", random.random()))
datum = datum[2:]

# dagteller = xxx

teller = random.randrange(1, 998)

# controlegetal = cc

controle = datum + str(teller)
controle = controle.replace('.', '')
controle = int(controle)

c = 97 - (controle % 97)

# rijksregister = jj.mm.dd-xxx.cc

print(f'{datum}-{teller:03d}.{c}')
